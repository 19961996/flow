﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{    private class Stone
    {
        private string _name;
        private double _price;
        private double _weight;
        private string _color;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        public double price
        {
            get { return _price; }
            set { _price = value; }
        }
        public double weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public string color
        {
            get { return _color; }
            set { _color = value; }
        }
    }
class Precious : Stone
{
    public double Hardness;
    public double Transparency;

    public double _price;
    public double _weight;
    public string _name;
    public string _color;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public double Price
    {
        get { return _price; }
        set { _price = value; }
    }

    public double Weight
    {
        get { return _weight; }
        set { _weight = value; }
    }
    public string Color
    {
        get { return _color; }
        set { _color = value; }
    }
}
class Semiprecious : Stone
{
    public string shine;
    public double density;

    public double _price;
    public double _weight;
    public string _name;
    public string _color;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public double Price
    {
        get { return _price; }
        set { _price = value; }
    }

    public double Weight
    {
        get { return _weight; }
        set { _weight = value; }
    }
    public string Color
    {
        get { return _color; }
        set { _color = value; }
    }
}


    public class Class1
    {

    }
}
